var net = require('net');
var sleep = require('sleep')

var HOST = '127.0.0.1';
var PORT = 8009;
var seq = 1;
var client = new net.Socket();
client.connect(PORT, HOST, function() {
    console.log('CONNECTED TO: ' + HOST + ':' + PORT);
    client.write('Seq=' + seq++);
});
    
client.on('data', function(data) {
    console.log('Resp: ' + data);
    client.write('Seq=' + seq++);
    sleep.sleep(1);
});

client.on('close', function() {
    console.log('Connection closed');
});
