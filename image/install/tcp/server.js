var net = require('net');

var HOST = '127.0.0.1';
var PORT = process.argv[2];

net.createServer(function(sock) {
    try{
    console.log('CONNECTED: ' + sock.remoteAddress +':'+ sock.remotePort);
    sock.on('data', function(data) {
        console.log('DATA ' + sock.remoteAddress + ': ' + data);
        try{
            sock.write('(from server port ' + PORT + ')' + 'You said "' + data + '"');
        }catch(e){
        }
    });
    sock.on('close', function(data) {
        console.log('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort);
    });
    
    sock.on('error', function(error){  } );
    }catch(e){
    }
}).listen(PORT, HOST);

console.log('Server listening on ' + HOST +':'+ PORT);
